const express = require('express');
const faker = require('faker');
const cors = require('cors');
const bodyParser = require('body-parser');

const videos = [
  'https://www.youtube.com/watch?v=8UVNT4wvIGY',
  'https://www.youtube.com/watch?v=KWZGAExj-es',
  'https://www.youtube.com/watch?v=BC2dRkm8ATU',
  'https://www.youtube.com/watch?v=nxg4C365LbQ',
  'https://www.youtube.com/watch?v=52Gg9CqhbP8',
  'https://www.youtube.com/watch?v=9RHFFeQ2tu4',
  'https://www.youtube.com/watch?v=52Gg9CqhbP8'
];

const app = express();
app.use(cors({origin: '*'}));
app.use(bodyParser.json());
app.get('/getVideos', (req, res) => {
  const data = [];
  for (let i = 0; i < 10; i++) {
    let question = faker.lorem.sentence(parseInt(Math.random() * 5) + 3) + '?';
    data.push({
      url: videos[parseInt(Math.random() * videos.length)],
      question
    });
  }
  res.send(data);
});

app.post('/results', (req, res) => {
  console.log(req.body);
  res.send('hello world');
});

app.listen(5000, () => console.log('it works'));
