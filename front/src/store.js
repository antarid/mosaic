import {createStore} from 'redux';

const question = {
  question: 'asdf',
  url: '',
  id: ''
};

const result = {
  answer: 'yes/no',
  question: ''
};

const initialState = {
  questions: [],
  currentVideo: 0,
  results: [],
  display: 'none'
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'LOAD_QUESTIONS':
      return {...state, questions: action.questions, display: 'questions'};
    case 'ANSWER_YES':
      return {
        ...state,
        results: [
          ...state.results,
          {
            question: state.questions[state.currentVideo].question,
            answer: 'yes'
          }
        ],
        currentVideo: state.currentVideo + 1,
        display:
          state.currentVideo >= state.questions.length - 1
            ? 'results'
            : 'questions'
      };
    case 'ANSWER_NO':
      return {
        ...state,
        results: [
          ...state.results,
          {question: state.questions[state.currentVideo].question, answer: 'no'}
        ],
        currentVideo: state.currentVideo + 1,
        display:
          state.currentVideo >= state.questions.length - 1
            ? 'results'
            : 'questions'
      };
  }
};

const store = createStore(reducer);
store.subscribe(() => console.log(store.getState()));
export default store;
