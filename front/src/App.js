import React, {Component} from 'react';
import './bootstrap/bootstrap.min.css';
import Main from './components/Main';

class App extends Component {
  render() {
    return <Main />;
  }
}

export default App;
