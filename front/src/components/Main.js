import React from 'react';
import Player from './Player';
import {Grid, Col, Row, Button} from 'react-bootstrap';
import _ from 'lodash';
import axios from 'axios';
import {connect} from 'react-redux';

const Statistics = ({answers}) => {
  axios
    .post('http://localhost:5000/results', answers)
    .then(res => console.log(res));
  return (
    <Grid>
      {answers.map(answer => (
        <Row>
          <Col md={6}>{answer.question}</Col>
          <Col md={6}>{answer.answer}</Col>
        </Row>
      ))}
    </Grid>
  );
};

class Main extends React.Component {
  componentDidMount() {
    fetch('http://localhost:5000/getVideos')
      .then(res => res.json())
      .then(questions => this.props.loadQuestions(questions));
  }
  render() {
    const {display} = this.props;
    console.log(this.props);
    switch (display) {
      case 'none':
        return null;
      case 'questions':
        const url = this.props.questions[this.props.currentVideo].url;
        const question = this.props.questions[this.props.currentVideo].question;
        return (
          <Grid style={{marginTop: '50px'}}>
            <Row>
              <h1 style={{textAlign: 'center', width: '100%'}}>{question}</h1>
              <Col md={12}>
                <Player url={url} />
              </Col>
              <Col md={6} style={{marginTop: '50px'}}>
                <Button bsStyle="danger" block onClick={this.props.answerNo}>
                  no
                </Button>
              </Col>
              <Col md={6} style={{marginTop: '50px'}}>
                <Button bsStyle="success" block onClick={this.props.answerYes}>
                  yes
                </Button>
              </Col>
            </Row>
          </Grid>
        );
      case 'results':
        return <Statistics answers={this.props.results} />;
      default:
        return null;
    }
  }
}

export default connect(
  state => {
    return {...state};
  },
  dispatch => ({
    loadQuestions: questions => dispatch({type: 'LOAD_QUESTIONS', questions}),
    answerYes: () => dispatch({type: 'ANSWER_YES'}),
    answerNo: () => dispatch({type: 'ANSWER_NO'})
  })
)(Main);
