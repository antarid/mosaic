import React from 'react';
import ReactPlayer from 'react-player';

const Player = ({url}) => <ReactPlayer width="100%" url={url} playing />;

export default Player;
